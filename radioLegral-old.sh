#!/bin/bash
PROJETNOM=${0##*/}


#### colorisation #############################################
BOLD=`tput smso`
NOBOLD=`tput rmso`

BLACK=`tput setaf 0`
#BLACK="\[\033[01;30m\]"

RED=`tput setaf 1`
#RED="\[\033[01;31m\]"

GREEN=`tput setaf 2`
#GREEN="\[\033[01;32m\]"

YELLOW=`tput setaf 3`
#YELLOW="\[\033[01;33m\]"

CYAN=`tput setaf 4`
#CYAN="\[\033[01;34m\]"

MAGENTA=`tput setaf 5`
#MAGENTA="\[\033[01;35m\]"

BLUE=`tput setaf 6`
#BLUE="\[\033[01;36m\]"

WHITE=`tput setaf 7`
#WHITE="\[\033[01;37m\]"

couleurNORMAL=$WHITE
couleurINFO=$BLUE
couleurCOM=$YELLOW
couleurWARN=$RED
couleurTITRE1=$GREEN
couleurTITRE2=$MAGENTA

export BOLD
export NOBOLD
export BLACK
export RED
export GREEN
export YELLOW
export CYAN
export MAGENTA
export BLUE
export WHITE

export couleurNORMAL
export couleurINFO
export couleurCOM
export couleurWARN
export couleurTITRE1
export couleurTITRE2


PROJETNOM=${0##*/}

#clear;
echo "${couleurINFO}################"
echo " $PROJETNOM"
echo "################${couleurNORMAL}"

SCRIPT_ROO=`dirname $0`;
SCRIPT_ROOT="${SCRIPT_ROO}/";
PLAYLISTS_ROOT="${SCRIPT_ROOT}playlists/" # chemin relatif au script


if [ -d /mediatheques/playlists/ ]; then
    PLAYLISTS_ROOT="/mediatheques/playlists/"     
fi


##### var interne
pc="" # PlaylistCall
pcLast="" # derniere playlist joue
pcLastFile="~/radioLegral.last"; # bug!
#pcLastFile="${SCRIPT_ROOT}radioLegral.last"; #ok
pcLastFile="/run/shm/radioLegral.last";	# dans un tempfs en rw

if [ $# -eq 0 ]; then
       # pas d'argument donner ->afficher l'usage
	echo "${couleurINFO}usage: $0 radio/playlist${couleurNORMAL}"

        echo "${couleurINFO}commandes:${couleurNORMAL}"

	echo "  --install  > cp radioLegral.sh dans /usr/bin/";

	echo "  last  > rejoue la dernier playlist";
	echo "  stop  > stop toutes les instances de vlc";

	echo "${couleurINFO}radios:${couleurNORMAL}";
        echo "  fi    > France Inter"
        echo "  finfo > France Info"
        echo "  fc    > France Culture"
    	echo "  rc    > radio Classique ";
        echo "  fm    > France Musique";
        echo "  rcf    > Radio Chretienne Francophone";
        echo "  fip   > FIP http://www.tv-radio.com/station/fip_mp3/fip_mp3-128k.m3u";
#       echo "  mouv  > Le Mouv";
        echo "  fbb    > France Bleu bourgogne";

        echo "  ici"
        echo "  courtoisie"
        echo "  mt";

        echo "${couleurINFO}les playlists${couleurNORMAL}"
        echo "  jeux           > playlist de musique de jeux"
        echo "  films           > playlist de musique de films"
        echo "  mangas         > playlist de musique de mangas"
        echo "  classiques     > playlist de musique classique"
        echo "  ambiances      > playlist de musique d ambiances tous genres confondus"
        echo "  bienetre       > playlist de musique d ambiance relaxante"
        echo "  angoisses       > playlist de musique d ambiance angoissante"
        echo "  calme          > playlist de musique calme"
        echo "  les_maitres_du_mysteres   > playlist de live audios: les maitre du mysteres "
        echo " "
        echo "  nuit_noire                > playlist d'histoire radiophoniques"
        echo "  PhilippeMeyer             > Les chroniques de Philippe Meyer"
        echo "  sur_les_epaules_de_Darwin > sur les epaules de Darwin"

        echo " "
        echo "  calmeFF7       > playlist de musique calme + Final Fantasy7"
        echo "  groupes        > playlist de musique dez groupes de musiques "
        echo "  fantasiles      > fantasile: POC, fatal Picard, Oldelaf"

        echo "  radioRivendell,rr > radioRivendell"
        echo "  radioGangrel, rl, nas, mediatheques  > radio de la mediatheque "

        echo "  carillon1, coucou3coups > bruitages";

        echo "repertoire du script: ${SCRIPT_ROOT}"
        echo "repertoire des playlists: ${PLAYLISTS_ROOT}"

        exit 1
fi


# - musique - #
DO[0]=32.70;   DOd[0]=34.65;   RE[0]=36.71;   REd[0]=38.89;   MI[0]=41.20;   FA[0]=43.65;   FAd[0]=46.25;   SOL[0]=49.00;   SOLd[0]=51.91;   LA[0]=55.00;   LAd[0]=58.27;   SI[0]=61.74;
DO[1]=65.41;   DOd[1]=69.30;   RE[1]=73.42;   REd[1]=77.78;   MI[1]=82.41;   FA[1]=87.31;   FAd[1]=92.50;   SOL[1]=98.00;   SOLd[1]=103.83;  LA[1]=110.00;  LAd[1]=116.54;  SI[1]=123.47;
DO[2]=130.81;  DOd[2]=138.59;  RE[2]=146.83;  REd[2]=155.56;  MI[2]=164.81;  FA[2]=174.61;  FAd[2]=185.00;  SOL[2]=196.00;  SOLd[2]=207.65;  LA[2]=220.00;  LAd[2]=233.08;  SI[2]=246.94;
DO[3]=261.63;  DOd[3]=277.18;  RE[3]=293.66;  REd[3]=311.13;  MI[3]=329.63;  FA[3]=349.23;  FAd[3]=369.99;  SOL[3]=392.00;  SOLd[3]=415.30;  LA[3]=440.00;  LAd[3]=466.16;  SI[3]=493.88;
DO[4]=523.25;  DOd[4]=554.37;  RE[4]=587.33;  REd[4]=622.25;  MI[4]=659.26;  FA[4]=698.46;  FAd[4]=739.99;  SOL[4]=783.99;  SOLd[4]=830.61;  LA[4]=880.00;  LAd[4]=932.33;  SI[4]=987.77;
DO[5]=1046.50; DOd[5]=1108.73; RE[5]=1174.66; REd[5]=1244.51; MI[5]=1318.51; FA[5]=1396.91; FAd[5]=1479.98; SOL[5]=1567.98; SOLd[5]=1661.22; LA[5]=1760.00; LAd[5]=1864.66; SI[5]=1975.53;
DO[6]=2093.00; DOd[6]=2217.46; RE[6]=2349.32; REd[6]=2489.02; MI[6]=2637.02; FA[6]=2793.83; FAd[6]=2959.96; SOL[6]=3135.96; SOLd[6]=3322.44; LA[6]=3520.00; LAd[6]=3729.31; SI[6]=3951.07;
DO[7]=4186.01; DOd[7]=4434.92; RE[7]=4698.64; REd[7]=4978.03; MI[7]=5274.04; FA[7]=5587.65; FAd[7]=5919.91; SOL[7]=6271.93; SOLd[7]=6644.88; LA[7]=7040.00; LAd[7]=7458.62; SI[7]=7902.13;

gamme () {
    delai=0.15
    #for gamme in 0 1 2 3 4 5 6 7
    for gamme in 7
    do
        echo "gamme: $gamme; note  ${DO[gamme]} ${DOd[gamme]} ${RE[gamme]} ${REd[gamme]} ${MI[gamme]} ${FA[gamme]} ${FAd[gamme]} ${SOL[gamme]} ${SOLd[gamme]} ${LA[gamme]} ${LAd[gamme]} ${SI[gamme]}"
        echo " note  DO   ${DO[gamme]}"; play --no-show-progress  --null synth $delai pluck ${DO[gamme]}
        echo " note  DO#  ${DOd[gamme]}"; play --no-show-progress  --null synth $delai pluck ${DOd[gamme]}
        echo " note  RE   ${RE[gamme]}"; play --no-show-progress  --null synth $delai pluck ${RE[gamme]}
        echo " note  RE#  ${REd[gamme]}"; play --no-show-progress  --null synth $delai pluck ${REd[gamme]}
        echo " note  MI   ${MI[gamme]}"; play --no-show-progress  --null synth $delai pluck ${MI[gamme]}
        echo " note  FA   ${FA[gamme]}"; play --no-show-progress  --null synth $delai pluck ${FA[gamme]}
        echo " note  FA#  ${FAd[gamme]}"; play --no-show-progress  --null synth $delai pluck ${FAd[gamme]}
        echo " note  SOL  ${SOL[gamme]}"; play --no-show-progress  --null synth $delai pluck ${SOL[gamme]}
        echo " note  SOL# ${SOLd[gamme]}"; play --no-show-progress  --null synth $delai pluck ${SOLd[gamme]}
        echo " note  LA   ${LA[gamme]}"; play --no-show-progress  --null synth $delai pluck ${LA[gamme]}
        echo " note  LA#  ${LAd[gamme]}"; play --no-show-progress  --null synth $delai pluck ${LAd[gamme]}
        echo " note  SI   ${SI[gamme]}"; play --no-show-progress  --null synth $delai pluck ${SI[gamme]}
    done
}

carillon1 (){
    gamme=5
    delai=1
    play --no-show-progress  --null synth $delai pluck ${MI[gamme]}
    play --no-show-progress  --null synth $delai pluck ${MI[gamme-1]}
    play --no-show-progress  --null synth $delai pluck ${MI[gamme]}
}

if [ -d $PLAYLISTS_ROOT ]; then
    echo "on va dans le rep de la playlist"
    cd $PLAYLISTS_ROOT
fi

# - vlc option - #

m3u=""
vlc_options=" --play-and-exit "
# Normaliseur de volume
#      --norm-buff-size <Entier [-2147483648 .. 2147483647]>
#      --norm-max-level <Flottant>
#vlc_options="${vlc_options} --network-caching 3000  --norm-max-level 1"

#command par defaut : lancer cvlc
cmdLine="cvlc";


# - selection de la playlist - #

pc=$1;	#pc: playlist call

# - selection de la derniere playlist si demander - #
if [ $pc == 'last' ]; then
	
	echo "derniere playlist demande";
	if [ -f $pcLastFile ]; then
	        echo "chargement de la derniere playlist"
        	pc=`cat $pcLastFile`;
		echo "derniere commande: ${pc}"
	else
		echo "${couleurWARN}le fichier $pcLastFile n'existe pas!${couleurNORMAL}";
		exit 1;
	fi
fi

case $pc in

    # - install - #
    --install )
        if [[ $EUID -ne 0 ]]; then
            echo "This script must be run as root" 1>&2
            exit 1
        fi


        echo "${couleurWARN}copie$couleurNORMAL";
        cmd="cp ${SCRIPT_ROOT}radioLegral.sh /usr/bin/radioLegral.sh";echo $cmd;eval $cmd;
	exit 1;
        ;;


    # - stopper les instances - #
    stop )
	cmdLine="stop";
	;;

    # - carillon - #
    carillon1 )
        cmdLine="carillon1";
        ;;
    coucou3coups )
        cmdLine="coucou3coups";
        ;;


    # - les radios - #
    franceInter | fi )
        cmdLine="cvlc";
        m3u="http://audio.scdn.arkena.com/11008/franceinter-midfi128.mp3"
        ;;
    franceInfo | finfo )
        cmdLine="cvlc";
        m3u="http://audio.scdn.arkena.com/11006/franceinfo-midfi128.mp3"
        ;;
    franceCulture | fc )
        cmdLine="cvlc";
        m3u="http://audio.scdn.arkena.com/11011/franceculture-lofi32.mp3"
        ;;
    franceMusique | fm )
        cmdLine="cvlc";
        m3u="http://audio.scdn.arkena.com/11012/francemusique-midfi128.mp3"
        ;;

    radioClassique | rc )
        cmdLine="cvlc";
        m3u="http://radioclassique.ice.infomaniak.ch/radioclassique-high.mp3";
        ;;


    rcf )
        cmdLine="cvlc";
        m3u="http://rcf.streamakaci.com/rcf.mp3";

        ;;

    fbb )
        cmdLine="cvlc";
        m3u="http://audio.scdn.arkena.com/11368/fbbourgogne-midfi128.mp3";
        ;;

    ici )
        cmdLine="cvlc";
        m3u="http://radio.rim952.fr:8000/stream.mp3"
        ;;

    courtoisie )
        cmdLine="cvlc";
        m3u="http://radiocourtoisie.nex-informatique.com/;stream.nsv"
        ;;

    mt )
        cmdLine="cvlc";
        m3u="http://192.168.100.32:8080/mt.ogg";
        ;;



   # - les playlists - #
   anglais )
         cmdLine="cvlc";
        cd ~
        m3u="anglais.m3u"
        vlc_options="${vlc_options} --random"
         ;;

   # - ambiances - #
   nas-ambiances | ambiances )
         cmdLine="cvlc";
        m3u="${PLAYLISTS_ROOT}nas-ambiances.m3u"
        vlc_options="${vlc_options} --random "
        ;;

   nas-bienetre | bienetre )
         cmdLine="cvlc";
        m3u="${PLAYLISTS_ROOT}nas-ambiances-bienEtre.m3u"
        vlc_options="${vlc_options} --random "
        ;;

   nas-angoisses | angoisses )
         cmdLine="cvlc";
        m3u="${PLAYLISTS_ROOT}nas-ambiances-angoisses.m3u"
        vlc_options="${vlc_options} --random "
        ;;

   calme )
         cmdLine="cvlc";
        m3u="calme.m3u"
        vlc_options="${vlc_options} --random"
         break
         ;;

   calmeFF7 )
         cmdLine="cvlc";
        m3u="calme,ff7.m3u"
        vlc_options="${vlc_options} --random"
         ;;

   nas-classiques | classiques )
         cmdLine="cvlc";
        m3u="${PLAYLISTS_ROOT}nas-classiques.m3u"
        vlc_options="${vlc_options} --random "
        ;;

   nas-films | films )
         cmdLine="cvlc";
        m3u="${PLAYLISTS_ROOT}nas-films.m3u"
        vlc_options="${vlc_options} --random "
        ;;

   nas-jeux )
         cmdLine="cvlc";
        m3u="${PLAYLISTS_ROOT}nas-jeux.m3u"
        vlc_options="${vlc_options} --random "
        ;;
   jeux )
         cmdLine="cvlc";
        m3u="${PLAYLISTS_ROOT}jeux.m3u"
        vlc_options="${vlc_options} --random "
        ;;

   nas-mangas | mangas )
        cmdLine="cvlc";
        m3u="${PLAYLISTS_ROOT}nas-mangas.m3u"
        vlc_options="${vlc_options} --random "
        ;;

   nas-groupes | groupes )
        cmdLine="cvlc";
        m3u="${PLAYLISTS_ROOT}nas-groupes.m3u"
        vlc_options="${vlc_options} --random "
        ;;

   radioRivendell | rr )
        cmdLine="cvlc";
        #m3u="http://88.191.136.226:8003/listen.pls"
        #m3u="http://195.154.242.152:8003/listen.pls"
        m3u='http://www.radiorivendell.com/playlist-128.m3u';
        ;;

   nas-fantasiles | fantasiles )
        cmdLine="cvlc";
        m3u="${PLAYLISTS_ROOT}nas-fantasiles.m3u"
        vlc_options="${vlc_options} --random"
        ;;

   nas-les_maitres_du_mysteres | les_maitres_du_mysteres )
        cmdLine="cvlc";
        m3u="${PLAYLISTS_ROOT}nas-les_maitres_du_mysteres.m3u"
        vlc_options="${vlc_options} --random"
        ;;

   nas-nuit_noire | nuit_noire )
        cmdLine="cvlc";
        m3u="${PLAYLISTS_ROOT}nas-nuit_noire.m3u"
        vlc_options="${vlc_options} --random"
        ;;

   nas-PhilippeMeyer | PhilippeMeyer )
        cmdLine="cvlc";
        m3u="${PLAYLISTS_ROOT}PhilippeMeyer.m3u"
        vlc_options="${vlc_options} --random"
        ;;

   nas-sur_les_epaules_de_Darwin | sur_les_epaules_de_Darwin )
        cmdLine="cvlc";
        m3u="${PLAYLISTS_ROOT}sur_les_epaules_de_Darwin.m3u"
        vlc_options="${vlc_options} --random"
        ;;

   radioGangrel  | rl | nas )
        cmdLine="cvlc";
        m3u="http://192.168.100.99:8100"
        # Normaliseur de volume
        # --norm-buff-size <Entier [-2147483648 .. 2147483647]>
        # Nombre de tampons audio
        # --norm-max-level <Flottant>

        vlc_options="${vlc_options} --network-caching 3000  --norm-max-level 1"
        ;;
                                        
    *)

         cmdLine="";
         ;;
  esac
#done


# - configuration de la commande preselectionnee - #

if [ "${cmdLine}" != "" ]; then
#        echo    "> ***************************************************************************************<"

       # - execution de la commande preselectionnee - #

	case "${cmdLine}" in


		stop )
			cmdLine="killall vlc";
			;;

		cvlc )
			echo    "${couleurINFO}suppression des instances presente de vlc${couleurNORMAL}"
			#killall vlc;
		
		        m3u="${dir}${m3u}"
		        cmdLine="cvlc ${m3u} ${vlc_options}"

			echo  "${pc}" > $pcLastFile	# last playing playlist
			;;

		coucou3coups )
                        echo    "coucou3coups...";

			cvlc  --play-and-exit  /mediatheques/musiques/bruitages/coucou3coups.mp3
                        exit 1;
                        ;;


	        carillon1)
	                echo    "carillon...";
			carillon1;            
	                
			exit 1;
	                ;;

		*)
			echo "${couleurWARN}commande selectionnée inconnue.${couleurNORMAL}";
			exit 2;
			;;
		esac

	echo "Execution de '${cmdLine}'"
#	echo    "> ---------------------------------------------------------------------------------------<"
        run=`${cmdLine}`
        echo ${run}
#        echo    "> ***************************************************************************************<"
        exit 1
else
	echo "${couleurWARN}pas de commande selectionnee${couleurNORMAL}";

fi

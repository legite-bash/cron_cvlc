#!/bin/bash
set -u
declare -r SCRIPT_PATH=`realpath $0`;        # chemin  absolu complet du script rep + nom
declare -r SCRIPT_REP=`dirname $SCRIPT_PATH`;# repertoire absolu du script (pas de slash de fin)

################
# colorisation #
################
RESET_COLOR=$(tput sgr0)
BOLD=`tput smso`
NOBOLD=`tput rmso`

BLACK=`tput setaf 0`
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
CYAN=`tput setaf 4`
MAGENTA=`tput setaf 5`
BLUE=`tput setaf 6`
WHITE=`tput setaf 7`

NORMAL=$WHITE
INFO=$BLUE
CMD=$YELLOW
WARN=$RED
TITRE1=$GREEN
TITRE2=$MAGENTA
DEBUG_COLOR=$MAGENTA

evalCmd(){
    local ligneNu="";   if [ ! -z ${2+x} ]; then ligneNu="[$2]:"; fi
    local txt="";       if [ ! -z ${3+x} ]; then txt="$3"; fi

    echo "${DEBUG_COLOR}$ligneNu$CMD$1$NORMAL$txt";
    eval "$1";
    return $?
}

#cd $SCRIPT_REP

lanceTest(){
    clear;
    evalCmd "$SCRIPT_REP/radioLegral.sh $@"
}


#Test usage
lanceTest "";

# --update
lanceTest "--update";

# une librairie
#lanceTest "coucou3coups";

# un fichier video
lanceTest "/media/mediatheques/Rebelle.avi";

# 
#lanceTest "";


#!/bin/sh
export FLUX_FRANCEINTER="http://direct.franceinter.fr/live/franceinter-midfi.mp3"
export FLUX_FRANCECULTURE="http://direct.franceculture.fr/live/franceculture-midfi.mp3";
export FLUX_FRANCEINFO="http://direct.franceinfo.fr/live/franceinfo-midfi.mp3";
export FLUX_MUSIQUE="http://direct.francemusique.fr/live/francemusique-midfi.mp3";
export FLUX_FBB="http://direct.francebleu.fr/live/fbbourgogne-midfi.mp3";

export FLUX_RADIOCLASSIQUE="http://radioclassique.ice.infomaniak.ch/radioclassique-high.mp3";
export FLUX_RCF="http://rcf.streamakaci.com/rcf.mp3"

export FLUX_RADIORIVENDELL_M3U="http://www.radiorivendell.com/playlist-128.m3u";
export FLUX_RADIORIVENDELL="http://radiorivendell.ddns.net:8000/128kbit.mp3";

export FLUX_ICI="http://radio.rim952.fr:8000/stream.mp3";
export FLUX_RADIOCOURTOISIE="http://radiocourtoisie.nex-informatique.com/;stream.nsv";

export FLUX_MT_LAN="http://192.168.100.32:8080/mt.ogg";
export FLUX_RL_LAN="http://192.168.0.202:39100";

export FLUX_COUCOU3COUPS="/media/mediatheques/Musiques/Bruitages/coucou3coups.mp3";

export FLUX_RCL="http://radiocampuslorraine.com:8000/web";

export FLUX_RADIOWIKIFR="http://streaming.radionomy.com/RadioWikiFR";

#configFlux (){

##########################
# * CONFIGURE LES FLUX * #
##########################
fonction_Test(){
    echo "fonction Test"
    if [ $IS_ROOT -eq 0 ];then
        echo "Appeller par root."
    else
        echo "Appeller par un user."
    fi
}
fonctionsTbl['fonction_Test']=fonction_Test;

coucou3coups(){
    cmdProg="cvlc" # on force cvlc comme lecteur
    cmdOptsVLC=" --gain 2 --play-and-exit ";
    flux=$FLUX_COUCOU3COUPS;
}
fonctionsTbl['coucou3coups']=coucou3coups;


carillon1(){

    if [  -x "/usr/bin/play1" ];
    then
        echo "${WARN}le programme play contenu dans le paquet sox n'est pas installé"
        echo "aptitude install sox$NORMAL"
        return
    fi
    out="carillon1";
	cmdProg=". $SCRIPT_REP/gammes.sh;carillon1";
}
fonctionsTbl['carillon1']=carillon1;


# - les radios - #

franceInfo(){
    flux=$FLUX_FRANCEINFO
}
fonctionsTbl['franceInfo']=franceInfo;
fonctionsTbl['finfo']=franceInfo;

franceInter(){
    flux=$FLUX_FRANCEINTER
}
fonctionsTbl['franceInter']=franceInter;
fonctionsTbl['fi']=franceInter;

franceCulture(){
    flux=$FLUX_FRANCECULTURE
}
fonctionsTbl['franceCulture']=franceCulture;
fonctionsTbl['fc']=franceCulture;

franceMusique(){
    flux=$FLUX_FRANCEMUSIQUE
}
fonctionsTbl['franceMusique']=franceMusique;
fonctionsTbl['fm']=franceMusique;

radioClassique(){
    flux=$FLUX_RADIOCLASSIQUE;
}
fonctionsTbl['radioClassique']=radioClassique;
fonctionsTbl['rc']=radioClassique;

radioRivendell(){
    flux=$FLUX_RADIORIVENDELL
}
fonctionsTbl['radioRivendell']=radioRivendell;
fonctionsTbl['ri']=radioRivendell;

radioRivendell_M3U(){
    flux=$FLUX_RADIORIVENDELL_M3U
}
fonctionsTbl['radioRivendell_M3U']=radioRivendell_M3U;
fonctionsTbl['rr3']=radioRivendell_M3U;


rcf(){
    flux=$FLUX_RCF;
}
fonctionsTbl['rcf']=rcf;

fbb(){
    flux=$FLUX_FBB;
}
fonctionsTbl['fbb']=fbb;

ici(){
    flux=$FLUX_ICI
}
fonctionsTbl['ici']=ici;

courtoisie(){
    flux=$FLUX_RADIOCOURTOISIE
}
fonctionsTbl['courtoisie']=courtoisie;

radioWikiw(){
    flux=$FLUX_RADIOWIKIFR;
}
fonctionsTbl['radioWikiw']=radioWikiw;
fonctionsTbl['rw']=radioWikiw;


radioCampusLorraine(){
    flux=$FLUX_RCL;
}
fonctionsTbl['radioCampusLorraine']=radioCampusLorraine;
fonctionsTbl['rcl']=radioCampusLorraine;


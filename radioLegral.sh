#!/bin/bash
set -u
declare -r SCRIPT_PATH=`realpath $0`;        # chemin  absolu complet du script rep + nom
declare -r SCRIPT_REP=`dirname $SCRIPT_PATH`;# repertoire absolu du script (pas de slash de fin)
declare -r SCRIPT_FILENAME=${0##*/};         # nom.ext
declare -r SCRIPT_NAME=${SCRIPT_FILENAME%.*}   # uniquement le nom
declare -r SCRIPT_EXT="sh"                   # uniquement l'extention
#$?: retour de la derniere commande shell
#$LINENO

declare -r VERSION="v0.2.7";



################
# colorisation #
################
RESET_COLOR=$(tput sgr0)
BOLD=`tput smso`
NOBOLD=`tput rmso`

BLACK=`tput setaf 0`
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
CYAN=`tput setaf 4`
MAGENTA=`tput setaf 5`
BLUE=`tput setaf 6`
WHITE=`tput setaf 7`

NORMAL=$WHITE
INFO=$BLUE
CMD=$YELLOW
WARN=$RED
TITRE1=$GREEN
TITRE2=$MAGENTA
DEBUG_COLOR=$MAGENTA
UNDERLINEIN=""


####################
# codes de sorties #
####################
declare -r E_ARG_NONE=65


#############
# variables #
#    init   #
#############
# --- Definir si root ou user --- #
[ "$(id -u)" == "0" ] && declare -r IS_ROOT=1 || declare -r IS_ROOT=0;

# --- variables generiques --- #
argNb=$#;
isUpdate=0;                     # demande de update
verbose=0;
isDebug=0;
isDebugShowVars=0;              # montrer les variables

# --- variables des includes --- #
declare -r UPDATE_HTTP="http://updates.legite.org/radioLegral";

declare -r CONF_ROOT="$SCRIPT_REP";
declare -r CONF_FILENAME="flux.sh";
declare -r CONF_PATH="$CONF_ROOT/$CONF_FILENAME";

declare -r CONF_ETC_ROOT="/etc/legite";
declare -r CONF_ETC_FILENAME="flux.sh";
declare -r CONF_ETC_PATH="$CONF_ETC_ROOT/$CONF_ETC_FILENAME";

#declare -r CONF_USER_ROOT="/root/.legite";
declare -r CONF_USER_ROOT="$HOME/.legite";
declare -r CONF_USER_FILENAME="flux.sh";
declare -r CONF_USER_PATH="$CONF_USER_ROOT/$CONF_USER_FILENAME";

# le fichier origine est copier lors de chaque installation
declare -r CONF_ORI_FILENAME="flux.ori.sh";
declare -r CONF_ORI_PATH="$CONF_USER_ROOT/$CONF_ORI_FILENAME";
isConf=0;                       # Chargement d'un fichier de configuration donné en parametre
confFilePSP="";                 # nom du fichier de configuration a appaller
declare -r CONF_FILE_PRIORITE="$CONF_USER_ROOT/radioLegral.priorite";


# --- variables des parametres supplementaires --- #
PSP_sup="";                     # parametre supplementaire($@) => $1 $2 $n "$@""

declare -A fonctionsTbl;        # tableau contenant les fonctions (venant de PSP_sup)
isShowLibs=0;                   # affiche une liste des fonctions defini

# --- variables du projet --- #
declare -r PLAYLISTS_ROOT="$SCRIPT_REP/playlists" # chemin relatif au script

sortie=0; # demande de sortie du programme
# --- variables du projet --- #
declare -i isShowInfo=0;

out="stout";
#uri="";

NOFLUX="-";
declare -r FLUX_DEFAUT="$NOFLUX";
flux="$FLUX_DEFAUT";

shuffle=0;
stop=0;
play=0;
prev=0;
add=0;
insert=0;
next=0;
crop=0;
stopVLC=0;

#vars calculés
actionNb=0;     # nb d'action direct demandé (prev,next,stop,etc)
#action="";      # (var calculé)

declare -r PRIORITE_DEFAUT=5;
declare -i priorite;
priorite=$PRIORITE_DEFAUT;
prioriteLvl=0   #consigne dans le fichier
# - creation/lecture du fichier d'execution temporaire - #
#loadPrioriteLvl

cmdProg=""; #command par defaut : rien
cmdOptsMPD=" --host localhost --port 6600";
cmdOptsMPD="";
cmdOptsVLC=" --play-and-exit "
    # Normaliseur de volume
    #      --norm-buff-size <Entier [-2147483648 .. 2147483647]>
    #      --norm-max-level <Flottant>
    #cmdOptsVLC="$cmdOptsVLC --network-caching 3000  --norm-max-level 1"


# #################### #
# functions generiques #
# #################### #
usage (){
    if [ $argNb -eq 0 ];
    then
        echo "$SCRIPT_FILENAME [--showLibs] [--update] [--confFile=] [--out=] librairie|flux";
        echo "Fonctions de debug:";
        echo " -d --showVars --displayVars showVarsProjet ";

        echo "";
        echo "--update: update le programme legite-install.sh lui meme"
        echo "--confFile: execute le fichier donné en argument si existe"
        echo "--showLibs: Affiche la liste des librairies"

        echo "";
        echo "Fichiers de configuration"
        echo "$CONF_ETC_PATH: est écrasé à chaque installation et mise a jours (ne pas editer)"
        echo "$CONF_ORI_PATH: est un exemple de fichier de configuration"
        echo "$CONF_USER_PATH: se lance avec --conf"

        echo "";
        echo "programme de sortie:";
        echo " --out=[stout|cvlc|mpd|defini dans librairie] (default: stout=cvlc)"
        echo " --stopVLC (killall vlc)"

        echo "";
        echo "Priorité des flux";
        echo " Le flux est lu que si priorité >= prioriteLvl (par defaut prioriteLvl=$PRIORITE_DEFAUT)";
        echo " --priorite= --prioriteLvl=";

        echo "";
        echo "Commandes de Musique Player Demon";
        echo " --out=mpd --stop --play --shuffle -aipnc --add --insert --prev --next --crop librairie|flux";

        echo "";
        echo "librairie|flux:";
        echo "execute une librairie ou est lu en tant que flux";
        echo "Le flux peut etre un fichier,playlist ou un flux réseau.";

        echo "thinks to add MPD_HOST and MPD_PORT in ~/.bashrc"

        exit $E_ARG_NONE;
    fi
}


#########
# DEBUG #
#########
showVars(){
    if [ $isDebugShowVars -eq 1 ];
    then
        isDebugOld=$isDebug;
        isDebug=1;
        #showDebug " $LINENO"
        showDebug " argNb=$argNb"
        showDebug " IS_ROOT=$IS_ROOT"
        showDebug " isUpdate=$isUpdate"
        showDebug " verbose=$verbose"
        showDebug " isDebug=$isDebugOld"
        showDebug " isDebugShowVars=$isDebugShowVars"

        showDebug " UPDATE_HTTP=$UPDATE_HTTP";
        
        showDebug " isConf=$isConf"
        showDebug " confFilePSP=$confFilePSP";
        showDebug " CONF_ETC_ROOT=$CONF_ETC_ROOT";
        showDebug " CONF_ETC_FILENAME=$CONF_ETC_FILENAME";
        showDebug " CONF_ETC_PATH=$CONF_ETC_PATH"
        showDebug " CONF_USER_ROOT=$CONF_USER_ROOT";
        showDebug " CONF_USER_FILENAME=$CONF_USER_FILENAME";
        showDebug " CONF_USER_PATH=$CONF_USER_PATH";
        showDebug " CONF_ORI_FILENAME=$CONF_ORI_FILENAME";
        showDebug " CONF_ORI_PATH=$CONF_ORI_PATH";

        showDebug " PLAYLISTS_ROOT=$PLAYLISTS_ROOT";
        showDebug " isShowLibs=$isShowLibs"
        showDebug " PSP_sup=$PSP_sup"  # => flux

        isDebug=$isDebugOld;
    fi
}


#evalCmd($cmd $ligneNu $txt)
evalCmd(){
    local ligneNu="";   if [ ! -z ${2+x} ]; then ligneNu="[$2]:"; fi
    local txt="";       if [ ! -z ${3+x} ]; then txt="$3"; fi

    echo "${DEBUG_COLOR}$ligneNu$CMD$1$NORMAL$txt";
    eval "$1";
    return $?
}


#showDebug(texte)
showDebug(){
    if [ $isDebug -eq 1 ];
    then
        #ligneNo=$1;
        texte="$1";
        if [ $# -eq 2 ]; then
            texte=$2;
        fi
        echo "${DEBUG_COLOR}$texte$NORMAL"
    fi
}


################
# MISE A JOURS #
################
# telecharge la derniere version en supprimant l'ancienne version.
#updateProg src dest
updateProg(){
    if [ $isUpdate -eq 1 ]
    then
        src=$1; dest=$2
        echo "${INFO}Update de $src$NORMAL"
        if [ $IS_ROOT -eq 1 ]
        then
            evalCmd "cp '$dest' '$dest.bak'" $LINENO;     # Fait une sauvegarde
            evalCmd "rm '$dest'";

            evalCmd "wget -O $dest $src"
            evalCmd "chmod +x $dest"
            if [ $? -ne 0 ];
            then
                echo "${WARN}Erreur lors de l'instalation de $dest.$NORMAL"
                evalCmd "mv '$dest.bak' '$dest' " 
            fi

        else
            echo "${WARN}Seul root peut updater (ecrire dans /usr/local/bin/, /root/.legite/)"
        fi
    fi
}

# - telecharge et installe la derniere version ainsi que les fichiers de configurations par defauts - #
selfUpdate (){
    if [ $isUpdate -eq 1 ];
    then
        if [ $IS_ROOT -eq 1  ];
        then
            echo "version actuelle: $VERSION"
            if [ ! -d "$CONF_ETC_ROOT" ];then mkdir "$CONF_ETC_ROOT";fi
            if [ ! -d "$CONF_USER_ROOT" ];then mkdir "$CONF_USER_ROOT";fi
            echo "mise a jours du programme";
            updateProg "$UPDATE_HTTP/$SCRIPT_FILENAME"   "/usr/local/bin/$SCRIPT_FILENAME"
            updateProg "$UPDATE_HTTP/$CONF_ETC_FILENAME" "$CONF_ETC_PATH"
            updateProg "$UPDATE_HTTP/$CONF_ORI_FILENAME" "$CONF_ORI_PATH"
            #updateProg "$UPDATE_HTTP/CONF_USER_FILENAME" "$CONF_USER_PATH"
            updateProg "$UPDATE_HTTP/gammes.sh" "$CONF_ETC_ROOT/gammes.sh"

            # affiche la nouvelle version
            newVersion=$($SCRIPT_PATH -V);
            echo "Mise à jours de la version $VERSION vers la version: $newVersion."
        else
            echo "Vous devez etre root pour mettre a jours le programme."
        fi
    fi
}


###############################
# GESTION DE LA CONFIGURATION #
###############################
# creer et execute les fichiers locaux
# LocalFileExec(localName){
execFile(){
    showDebug "execFile($@)";
    local localName="$1";
    if [  -f "$localName" ];
    then
        if [ ! -x "$localName" ];then evalCmd "chmod +x $localName;"; fi
        #echo "nano $localName # Pour modifier le fichier"
        evalCmd ". $localName";
    else
        echo "${WARN}Le fichier '$localName' n'existe pas.$NORMAL"
    fi
}

# chargement di fichier de configuration defini par le PSP
loadConfFilePSP(){
    if [ $isConf -eq 1 ];
    then
        execFile "$confFilePSP"
    fi
}

##########################
# GESTION DES LIBRAIRIES #
##########################
showLibs(){
    if [ $isShowLibs -eq 1 ];
    then
        local out="";
        for value in "${fonctionsTbl[@]}";
        do
            out="$out $value"
        done;
        echo "$out";
    fi
}

isLibExist(){
    #showDebug "$LINENO: isLibExist()"
    local lib="$1"
    for value in "${fonctionsTbl[@]}";
    do
        if [ "$value" == "$lib" ]; then return 1; fi
    done;
    return 0;
}


# ################### #
# functions du projet #
# ################### #
showVarsProjet(){
    if [ $isDebugShowVars -eq 1 ];
    then
        isDebugOld=$isDebug;
        isDebug=1;
        #showDebug " $LINENO"

        showDebug " out=$out";

        showDebug " NOFLUX=$NOFLUX";
        showDebug " FLUX_DEFAUT=$FLUX_DEFAUT";
        showDebug " flux=$flux";


        showDebug " shuffle=$shuffle, stop=$stop, play=$play, prev=$prev, add=$add, insert=$insert, next=$next, crop=$crop"
        showDebug " stopVLC=$stopVLC"

        showDebug " actionNb=$actionNb";
        showDebug " PRIORITE_DEFAUT=$PRIORITE_DEFAUT";
        showDebug " priorite=$priorite";
        showDebug " prioriteLvl=$prioriteLvl";

        showDebug " cmdProg=$cmdProg";
        showDebug " cmdOptsMPD=$cmdOptsMPD";
        showDebug " cmdOptsVLC=$cmdOptsVLC"

        isDebug=$isDebugOld;
    fi
}


h1 (){
    echo $UNDERLINEIN$GREEN$1$NORMAL
}

h2 (){
    echo $UNDERLINEIN$MAGENTA$1$NORMAL
}

# - fonction executer a chaque sortie du programme - #
# $1: code retour a transmettre
sortie (){
    #echo "sortie"
    #displayVars
    exit $1
}


displayVar (){
    o="";
    while [ $# -gt 1 ]; do
        o="$o $INFO $1:$NORMAL$2"
        shift 2;
    done
    echo "$o"
}


displayVars (){
    h2 "commandes générales:"
    displayVar isUpdate $isUpdate

    h2 "Configuration:"
    displayVar CONF_FILE_PRIORITE $CONF_FILE_PRIORITE 

    h2 "variables:"
    displayVar flux $flux PRIORITE_DEFAUT $PRIORITE_DEFAUT priorite $priorite prioriteLvl $prioriteLvl

    h2 "Commandes:"
    displayVar cmdProg $cmdProg

    h2 "Commandes MPC:"
    displayVar stopVLC $stopVLC add $add insert $insert prev $prev next $next crop $crop shuffle $shuffle
}





# - gestion de la consigne de priorite - #
setPrioriteLvl (){

    echo "setPrioriteLvl ($1)";

    if [ $# -eq 0 ]
    then
        prioriteLvl=$PRIORITE_DEFAUT;
    else
        if [ $1 -ge 0 ]
        then
            prioriteLvl=$1;
            echo $prioriteLvl > $CONF_FILE_PRIORITE
        fi
    fi
}


loadPrioriteLvl (){
    # charge depuis le fichier
    showDebug "Chargement de la priorité via $CONF_FILE_PRIORITE";

    #if [ -s "$CONF_FILE_PRIORITE" ] 
    if [  -r "$CONF_FILE_PRIORITE" ] ;
    then
        showDebug "Le fichier $CONF_FILE_PRIORITE est lisible.";

        if [ ! -f  $CONF_FILE_PRIORITE ];
        then
            echo "$PRIORITE_DEFAUT" > $CONF_FILE_PRIORITE;
        fi

        prioriteLvl=`cat $CONF_FILE_PRIORITE`
        showDebug "prioriteLvl=$prioriteLvl";

        # verifie la valeur chargee
        if [ $prioriteLvl -lt 0 ] ;then
            prioriteLvl=$PRIORITE_DEFAUT;
        fi
    # pas de fichier lisible
    else
        prioriteLvl=$PRIORITE_DEFAUT
        echo "$prioriteLvl" > $CONF_FILE_PRIORITE
    fi
}


# execute la commande si priorite est au moins egal a prioriteLvl
execCmd(){
    showDebug "execCmd($@)"
    #if [ ! "$cmdProg" = "" ]
    #then
        if [ $priorite -ge $prioriteLvl  ]
        then
            evalCmd "$1";
        else
            echo "La priorité du flux ($priorite) est inferieur à la consigne ($prioriteLvl)";
        fi
    #fi
}


# ########## #
# parametres #
# ########## #
TEMP=`getopt \
     --options v::dVhaipnc \
     --long verbose::,debug,showVars,showLibs,version,help,update,showInfo,confFile::,out::,prioriteLvl::,priorite::,play,stop,shuffle,add,insert,prev,next,crop \
     -- "$@"`


# Note the quotes around `$TEMP': they are essential!
eval set -- "$TEMP"
#if [ $? -eq 0 ] ; then echo "options requise. Sortie" >&2 ; exit 1 ; fi
#echo "$couleurWARN nb: $# $couleurNORMAL";
#echo "${INFO}argNb:$NORMAL $#"
#echo "${INFO}arg:$NORMAL $?"

while true ; do
    case "$1" in

        # - fonctions generiques - #
        -h|--help) usage; exit 0; shift ;;
        -V|--version) echo "$VERSION"; exit 0; shift ;;
        -v|--verbose)
            case "$2" in
                "") verbosity=1; shift 2 ;;
                *)  #echo "Option c, argument \`$2'" ;
                verbosity=$2; shift 2;;
            esac ;;
        -d|--debug)
            isDebug=1;
            isDebugShowVars=1;
            shift
            ;;
        --showVars)isDebugShowVars=1;   shift ;;

        # - Mise a jours - #
        --update)  isUpdate=1;          shift ;;

        # - Librairies - #
        --showLibs) isShowLibs=1;         shift ;;

        # - Chargement d'un fichier de configuration - #
        --confFile)
            isConf=1;
            confFilePSP="$2";
            shift 2;;

        # - parametres liés au projet - #
        --showInfo)  isShowInfo=1;  shift ;;
            
        # - programme utilisé pour lire le flux - #
        --out)       out=$2; shift 2 ;;

        # changer le niveau globale de la priorite
        --prioriteLvl) setPrioriteLvl $2; shift 2  ;;

        # definir le priorite du flux
        --priorite)       priorite=$2; shift 2  ;;


        --play)      out="mpd"; play=1;   shift ;;
        --stop)      out="mpd"; stop=1;   shift ;;
        --shuffle)   out="mpd"; shuffle=1;shift ;;
        -a|--add)    out="mpd"; add=1;    shift ;;
        -i|--insert) out="mpd"; insert=1; shift ;;
        -p|--prev)   out="mpd"; prev=1;   shift ;;
        -n|--next)   out="mpd"; next=1;   shift ;;
        -c|--crop)   out="mpd"; crop=1;   shift ;;
       
        # - Les parametres supplementaires - #
        --)
            if [ -z ${2+x} ];
            then
                PSP_sup="";
            else
                PSP_sup="$2";
            fi
            shift ; break ;;

        *)
            echo "option $1 $2 non reconnu"; shift;
            exit 1 ;; # sans exit: si la derniere option est inconnu -> boucle sans fin
        esac
done


########
# main #
########
#clear;
#echo "$INFO####";
#echo "${INFO}SCRIPT_REP:$NORMAL $SCRIPT_REP";
echo "${INFO}$SCRIPT_PATH $VERSION$NORMAL";
#echo "${INFO}SCRIPT_FILENAME:$NORMAL $SCRIPT_FILENAME";
#echo "${INFO}SCRIPT_NAME:$NORMAL $SCRIPT_NAME";

# - 0- usage - #
usage;

# - 1- mise a jours du programme - #
selfUpdate;

# - Affichage des variables - #
showVars;

# - sortie en cas d'install/mise a jours - #
if [ $isUpdate -eq 1 ]; then
    exit 0;
fi


#############################################
# CHARGEMENT DES FICHIERS DE CONFIGURATIONS #
#############################################
execFile "$CONF_ETC_PATH";
execFile "$CONF_PATH";
execFile "$CONF_USER_PATH"
loadConfFilePSP


############################
# CHARGEMENT DELA PRIORITE #
############################
loadPrioriteLvl


#################################
# EXECUTION DES ACTIONS DIRECTS #
#################################
if [ $stopVLC -eq 1 ];
then
    evalCmd "killall vlc";
fi


############################
# AFFICHAGE DES LIBRAIRIES #
############################
showLibs;


############################
# TRAITEMENT DE LA PSP_SUP #
############################
if [ "$PSP_sup" != "" ];
then
    # est ce une librairie?
    isLibExist "$PSP_sup"
    if [ $? -eq 1 ];then
        showDebug "Chargement de la librairie:"
        evalCmd "$PSP_sup";
        # La librairie peut definir: flux, cmdProg, cmdOptsVLC
    else # si ce n'est pas une librairie alors:
        flux="$PSP_sup"; # on le considere comme un flux direct le out reste inchangé
    fi 
fi


#############################
# GESTION DES SORTIES (out) #
#############################
###################
# -- out:stout -- #
if [ "$out" = "stout" ]; then
    showDebug "out:$out"
    out="cvlc";
fi


###################
# -- out: cvlc -- #
if [ "$out" = "cvlc" ]; then
    showDebug "out:$out"
    cmdProg=$out;
fi


###################
# -- out:  mpd -- #
if [ "$out" = "mpd" ]
then
    echo "out= mpd"
    #cmdProg="mpc";

#    if [ $actionNb -eq 0 ]; then
#        if [ "$flux" != "" ];then
#            add=1; # add: action par defaut si 1 flux preciser 
#        fi
#    fi # if [ $actionNb ];

 
    # action immediate (sans flux)
    if [ $play -eq 1 ];   then execCmd "mpc $cmdOptsMPD play 1"; fi
    if [ $stop -eq 1 ];   then execCmd "mpc $cmdOptsMPD stop"; fi
    if [ $shuffle -eq 1 ];then execCmd "mpc $cmdOptsMPD shuffle"; fi
    if [ $prev -eq 1 ];   then execCmd "mpc $cmdOptsMPD prev"; fi
    if [ $next -eq 1 ];   then execCmd "mpc $cmdOptsMPD next"; fi
    if [ $crop -eq 1 ];   then execCmd "mpc $cmdOptsMPD crop"; fi
    # action immediate (avec flux)
    if [ $add -eq 1 ];    then execCmd "mpc $cmdOptsMPD add '$flux'"; fi
    if [ $insert -eq 1 ]; then execCmd "mpc $cmdOptsMPD insert '$flux'"; fi

    #sortie 1
    #cmdProg=""; #desactivation de la commande par defaut
fi #mpd


###############################
# - ROUTTAGE SELON $cmdProg - #
###############################
case "$cmdProg" in

	cvlc )
        if [ "$flux" = "$NOFLUX" ]
        then
            echo "pas de flux fournis.";
            cmdProg=""; #void
        else
            cmdProg="$cmdProg $cmdOptsVLC '$flux'";
            #echo  "$flux" > $pcLastFile	# last playing playlist
        fi
		;;
        
    *) # la librairie defini son programme a executer avec ses parametres via $cmdProg
        showDebug "$cmdProg";
        ;;
esac


#########################
# - afficher les info - #
#########################
showVarsProjet;

if [ $isShowInfo -eq 1 ]
then
    displayVars
fi


# - EXECUTION DE LA COMMANDE - #
execCmd "$cmdProg";